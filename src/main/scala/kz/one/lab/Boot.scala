package kz.one.lab

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import kz.one.lab.actors.ParentActor

object Boot extends App {

  val config = ConfigFactory.load()

  val system = ActorSystem(config.getString("actor.system.name"))

  val actorParentProps = Props(new ParentActor)

  val actorParent = system.actorOf(actorParentProps)
  val actorParent2 = system.actorOf(actorParentProps)

  actorParent ! "Hello"
  //actorParent2 ! ParentMsg(actorParent.path)
}
