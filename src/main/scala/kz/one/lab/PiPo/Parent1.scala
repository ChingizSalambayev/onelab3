package kz.one.lab.PiPo

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object Parent1{
  def props = Props(new Parent1)
}

class Parent1 extends Actor with ActorLogging{

  override def preStart(): Unit = {
    log.info(s"Parent class ${self.path} started")
  }

  override def postStop(): Unit = {
    log.info(s"Parent class ${self.path} stopped")
  }

  val nameOfChild: String = "child" + self.path.name.substring(self.path.name.length - 1, self.path.name.length)

  val childActor: ActorRef = context.actorOf(Child1.props, nameOfChild)
  val laps = 100
  var count = 0

  override def receive: Receive = {
    case sg: StartGame =>
      log.info(s"Game started")
      context.actorSelection(sg.actorRef.path.child("child2")) ! Ping()
//    case Ping(message) =>
//        log.info(s"       $message received, Sender: ${sender.path}")
//        context.actorSelection(sender.path.parent) ! Pong()
    case Pong(message) =>
      if(count == laps){
        context.actorSelection(self.path.parent.child("P1")) ! StopGame()
        context.actorSelection(self.path.parent.child("P2")) ! StopGame()
      } else {
        count += 1
        log.info(s"         $message received, Sender: ${sender.path}")
        if(sender.path.name == "child2") {
          context.actorSelection(self.path.parent.child("P1").child("child1")) ! Ping()
        } else {
          context.actorSelection(self.path.parent.child("P2").child("child2")) ! Ping()
        }
      }
    case StopGame(message) =>
      context.stop(self)
  }
}
