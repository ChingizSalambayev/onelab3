package kz.one.lab.PiPo

import akka.actor.{Actor, ActorLogging, Props}

object Child1{
  def props = Props(new Child1)
}

class Child1 extends Actor with ActorLogging{

  override def preStart(): Unit = {
    log.info(s"Child class ${self.path} started")
  }

  override def postStop(): Unit = {
    log.info(s"Child class ${self.path} stopped")
  }

  override def receive: Receive = {
    case Ping(message) =>
      log.info(s"$message received, Sender: ${sender.path}")
      context.parent ! Pong()
//    case Pong(message) =>
//      log.info(s"  $message received, Sender: ${sender.path}")
//      context.actorSelection(sender.path.parent) ! Ping()
  }
}
