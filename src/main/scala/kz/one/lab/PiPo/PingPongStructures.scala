package kz.one.lab.PiPo

import akka.actor.ActorRef

case class StartGame(message: String = "Go", actorRef: ActorRef)
case class Ping(message: String = "Ping")
case class Pong(message: String = "Pong")
case class StopGame(message: String = "Stop")