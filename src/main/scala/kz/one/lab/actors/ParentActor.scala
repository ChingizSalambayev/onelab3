package kz.one.lab.actors

import akka.actor.{Actor, ActorLogging, ActorPath, Props}
import kz.one.lab.actors.ParentActor.{ParentMsg, Work}
import kz.one.lab.actors.routers.Master

object ParentActor{

  case class ParentMsg(sendPath: ActorPath)
  case class Work(msg: String)
}

class ParentActor extends Actor with ActorLogging{

  override def preStart(): Unit = {
    log.info(s"ParentActor ${self.path}: preStart ")
  }

  override def postStop(): Unit = {
    log.info(s"ParentActor ${self.path}: postStop")
  }

  override def receive: Receive = {
    case "Hello" =>
      log.info(s"My ActorPath is: ${self.path}")
      val masterActor = context.actorOf(Props(new Master))
      var n:Int = 0
      while (n!=5){
        masterActor ! Work(s"TEST Router $n")
        n += 1
      }

      context.become(waitingResponse)

    case "OK" =>
      log.info(s"Sender is: ${sender().path}")
      context.stop(self)

    case msg: ParentMsg =>
      log.info(s"Received $msg")
      context.actorSelection(msg.sendPath) ! "OK"
    case x => log.info(s"Received Parent: $x")
  }

  def waitingResponse: Receive = {
    case x => log.info(s"Received Parent: $x")
  }

}