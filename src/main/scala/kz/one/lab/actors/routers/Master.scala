package kz.one.lab.actors.routers

import akka.actor.{Actor, ActorLogging, Props, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import kz.one.lab.actors.CholdActor
import kz.one.lab.actors.ParentActor.Work

class Master extends Actor with ActorLogging{
  var router = {
    val routees = Vector.fill(3) {
      val r = context.actorOf(Props[CholdActor])
      context.watch(r)
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  def receive = {
    case w: Work =>
      router.route(w, sender())
    case Terminated(a) =>
      router = router.removeRoutee(a)
      val r = context.actorOf(Props[CholdActor])
      context.watch(r)
      router = router.addRoutee(r)

    case x => log.info(s"Received Master: $x")
  }
}
