package kz.one.lab.actors

import akka.actor.{Actor, ActorLogging}
import kz.one.lab.actors.ParentActor.Work

object CholdActor{
}

class CholdActor extends Actor with ActorLogging{
/*
  override def preStart(): Unit = {
    log.info("CholdActor: preStart ")
  }

  override def postStop(): Unit = {
    log.info("CholdActor: postStop")
  }*/

  override def receive: Receive = {
    case "Bye" =>
      log.info("Received bye")
      sender() ! "OK"

    case w: Work =>
      log.info(s"ChildActor received msg: $w, path is: ${self.path}")
      sender() ! w.msg
  }
}
