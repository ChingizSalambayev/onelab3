package kz.one.lab

import akka.actor.ActorSystem
import kz.one.lab.PiPo.{Parent1, StartGame}

object PingPong extends App {
  val system = ActorSystem("PingPong")

  val parent1 = system.actorOf(Parent1.props, "P1")
  val parent2 = system.actorOf(Parent1.props, "P2")

  parent1 ! StartGame("Go", parent2)
}
